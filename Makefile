CC=g++
CXXFLAGS=-Wall -g
CPP_FILES := $(wildcard *.cpp)
OBJ_FILES := $(CPP_FILES:.cpp=.o)
main:$(OBJ_FILES)

clean:
	rm main *.o
