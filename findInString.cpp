#include <vector>
#include "functions.h"
std::vector<int> findInString(char search,const char* string){
	// Get length
	int length = characterCount(string);
	std::vector<int> positions;
	for(int i = 0; i < length; i++)
	{
		if(string[i] == search)
		{
			positions.push_back(i);
		}
	}
	return positions;
}
