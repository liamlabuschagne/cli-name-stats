#include <string>
#include <cstring>
#include "toLowerCase.h"
const char* reverse(const char* str)
{
	const char* tmp = toLowerCase(str);
	std::string out = "";
	for(int i = strlen(tmp)-1; i >= 0; i--){
		out = out + tmp[i];
	}
	
	return out.c_str();
}
