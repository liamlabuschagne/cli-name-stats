#include <cstring>
#include <ctype.h>
const char* toLowerCase(const char* str)
{
	char tmp[strlen(str) + 1];
	strcpy(tmp,str);
	for(int i = 0; tmp[i]; i++){
	  tmp[i] = tolower(tmp[i]);
	}
	const char* out = tmp;
	return out;
}
