#include <cstring>
#include <string>
#include <vector>
#include "findInString.h"
#include "toLowerCase.h"
const char* toPigLatin(const char* str)
{
	const char* tmp = toLowerCase(str);
	// Creating a list of the positions of the first vowels
	char vowels[5] = {'a','e','i','o','u'};
	int firstVowelPosition = -1;
	for(int i = 0; i < 5; i++)
	{
		char tmp2[strlen(tmp)];
		strcpy(tmp2,tmp);
		std::vector<int> positions = findInString(vowels[i],tmp2);
		if(positions[0] < firstVowelPosition || firstVowelPosition == -1)
			firstVowelPosition = positions[0];
		
	}
	
	
	std::string out = "";
	if(firstVowelPosition != -1){
		std::string constonantClump = "";
		// Get first constonant clump
		for(int i = 0; i < firstVowelPosition; i++)
		{
			constonantClump = constonantClump + str[i];
		}
		// Get the rest of the string
		std::string rest = "";
		for(unsigned i = firstVowelPosition; i < strlen(str); i++)
		{
			rest = rest + str[i];
		}
		out = rest + constonantClump + "ay";
	}
	return out.c_str();
}
