#include <ctype.h>
#include <cstring>
int vowelCount(const char*  str)
{
	char tmp[strlen(str) + 1];
	strcpy(tmp,str);
	// Make everything lower case for simplicity
	for(int i = 0; tmp[i]; i++){
	  tmp[i] = tolower(tmp[i]);
	}
	// Check for vowels
	int count = 0;
	
	for(int i = 0; tmp[i]; i++){
	  if(tmp[i] == 'a' || tmp[i] == 'e' || tmp[i] == 'i' || tmp[i] == 'o' || tmp[i] == 'u')
		count++;
	}
	
	return count;
}
